import Vue from 'vue';
import Vuex from 'vuex';

import state from './state';
import getters from './getter';
import mutations from './mutation';
import actions from './action';
import createLogger from 'vuex/dist/logger';
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';
console.log(process.env.NODE_ENV)

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  // 如果是开发模式, 使用createLogger插件打印state修改过程
  plugins: debug ? [createLogger()] : []
});
