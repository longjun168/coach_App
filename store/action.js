import mt from "./mutationType";
export default {
	[mt.TEST_MT] (ctx, value) {
		console.log(ctx);
		let {commit} = ctx;
		commit(mt.TEST_MT, value);
	}
}