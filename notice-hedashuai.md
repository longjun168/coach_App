

## 学生端页面都加了 Sc(StudnetClient 的缩写) 开头

* ScIndex							首页
* ScPersonalCenter		个人中心
* ScMyCourse			我的课程页面

## 渐变按钮组件
```HTML
<!-- 背景是从左到右的渐变按钮, 默认是 inline-block -->
import GradientButton from '@/components/gradientButton/gradientButton.vue';
<GradientButton>测试按钮</GradientButton>
<GradientButton style="width: 100%;">测试按钮</GradientButton>
```
## 底部固定TABAR 组件

```HTML
import BottomTabar from '@/components/bottomTabar/bottomTabar.vue';
<!-- 要传入一个数字, 这个数字是当前选中菜单的下标 -->
<BottomTabar :active="0"></BottomTabar>
```